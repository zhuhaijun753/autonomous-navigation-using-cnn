//
//  main.cpp
//  Feature
//
//  Created by vishakh duggal on 30/10/15.
//  Copyright © 2015 vishakh duggal. All rights reserved.
//


#include "ros/ros.h"
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>
#include <sensor_msgs/Image.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>

#include "filters.hpp"
#include "linear.h"
#include "predict.hpp"

using namespace cv;

std::string encoding;
image_transport::CameraPublisher pub;
image_transport::CameraSubscriber sub_camera;
camera_info_manager::CameraInfoManager *cinfo;

std::ofstream outfile;

void callback(const sensor_msgs::ImageConstPtr& image_msg, const sensor_msgs::CameraInfoConstPtr& info)
{
    cv::Mat frame, img;
    static bool first=true;
    cv::Mat gray;
    cv::Size size;
    cv::Mat feature_vector;
    cv::Mat predictedLabelMatrix;
    IplImage* iplImage;
    int rows, cols;
    double min, max;

    sensor_msgs::ImagePtr image_msg_ptr;
    sensor_msgs::CameraInfoPtr cinfo_msg(new sensor_msgs::CameraInfo(cinfo->getCameraInfo()));


    try
    {
        frame = cv_bridge::toCvShare(image_msg, encoding)->image;
    }
    catch(cv_bridge::Exception)
    {
        ROS_ERROR("Unable to convert %s image to %s", image_msg->encoding.c_str(), encoding.c_str());
        return;
    }

    if (!frame.empty()) {
        size = frame.size();
        rows = size.height;
        cols = size.width;
        feature_vector = applyfilters(frame, rows, cols);

        size = feature_vector.size();

        predictedLabelMatrix = predict_lables(feature_vector);
  
        //predictedLabelMatrix.convertTo(img, CV_32FC1); 
        //cv::imshow("Display window", img);
        //waitKey(0);
        //outfile<<predictedLabelMatrix;
        //image_msg_ptr = cv_bridge::CvImage(std_msgs::Header(), "mono8", img/21.0).toImageMsg();

        predictedLabelMatrix.convertTo(img, CV_8UC1);      
        image_msg_ptr = cv_bridge::CvImage(std_msgs::Header(), "mono8", predictedLabelMatrix).toImageMsg();

        cinfo_msg->header.stamp = image_msg_ptr->header.stamp;
        cinfo_msg->header.frame_id = image_msg_ptr->header.frame_id;
        cinfo_msg->width = image_msg_ptr->width;
        cinfo_msg->height = image_msg_ptr->height;
        pub.publish(image_msg_ptr, cinfo_msg);

    } else {
        ROS_WARN("Frame skipped, no data!");
    }

    return;
}

int main ( int argc, char** argv )
{

    ros::init(argc, argv, "depth_estimation", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    image_transport::ImageTransport it(nh);
 
    //outfile.open("cameraFrame.csv");

   /* Publishing the depthImage */
    pub = it.advertiseCamera("/depth_estimation/image", 1);

    /* Receiving the RGB video  stream*/
    std::string topic = nh.resolveName("/bebop/front/image_raw");
    sub_camera = it.subscribeCamera(topic, 1, &callback);
    image_transport::Subscriber sub_image = it.subscribe(topic, 1,
                                                         boost::bind(callback, _1, sensor_msgs::CameraInfoConstPtr()));

    cinfo = new camera_info_manager::CameraInfoManager(ros::NodeHandle("/bebop/front"), "bebop_front");
    
    ros::spin();

    //outfile.close();
    return 0;
}
