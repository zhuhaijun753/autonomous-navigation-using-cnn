//
//  filters.cpp
//  Feature
//
//  Created by vishakh duggal on 30/10/15.
//  Copyright © 2015 vishakh duggal. All rights reserved.
//

#include "filters.hpp"
#include "iostream"
#include "conv2D.hpp"



int nLaw = 17;
float MatrixConditioner = .2;
float X[] ={1.0, 2.0, 1.0};
cv::Mat L3 = cv::Mat(1, 3, CV_32F,X) / 128.0 / MatrixConditioner;
float X1[] ={-1.0, 0.0, 1.0};
cv::Mat E3 = cv::Mat(1, 3, CV_32F,X1);
float X2[] = {-1.0, 2.0, -1.0};
cv::Mat S3 = cv::Mat(1, 3, CV_32F,X2);
float D[5][5] = {{-100.0, -100.0, 0.0, 100.0, 100.0}, {-100.0, -100.0, 0.0, 100.0, 100.0}, {-100.0, -100.0, 0.0, 100.0, 100.0},{-100.0, -100.0, 0, 100.0, 100.0}, {-100.0, -100.0, 0.0, 100.0, 100.0}};
cv::Mat NB1 = cv::Mat(5,5,CV_32F,D)/2000.0;
float E [5][5] = {{-100.0, 32.0, 100.0, 100.0, 100.0}, {-100.0, -78.0, 92.0, 100.0, 100.0}, {-100.0, -100.0, 0.0, 100.0, 100.0},{-100.0, -100.0, -92.0, 78.0, 100.0}, {-100.0, -100.0, -100.0, -32.0, 100.0}};
cv::Mat NB2 = cv::Mat(5,5,CV_32F,E)/2000.0;


cv::Mat NB3 = -NB2.t();
cv:: Mat NB4 = -NB1.t();
float F[5][5] = {{ 0.05,   0.05,   0.05,   0.05,   0.05, },
    { 0.05,   0.05,   0.05,   0.039, -0.016},
    { 0.05,   0.046,  0.0,    -0.046, -0.05 },
    { 0.016, -0.039, -0.05,  -0.05,  -0.05 },
    {-0.05,  -0.05,  -0.05,  -0.05,  -0.05 }};
cv:: Mat NB5 = cv::Mat(5,5,CV_32F,F);

cv:: Mat NB6 = NB5.t();

float X3[3][3] = {{ 0.00152588,  0.00305176 , 0.00152588},
    { 0.00305176 , 0.00610352,  0.00305176},
    { 0.00152588,  0.00305176,  0.00152588}};


float X4 [3][3] = {{-0.0390625,  0.0,         0.0390625},
    {-0.078125,   0.0,         0.078125 },
    {-0.0390625,  0.0,         0.0390625}};

float X5 [3][3] = {{-0.0390625,  0.078125,  -0.0390625},
    {-0.078125,   0.15625,   -0.078125 },
    {-0.0390625,  0.078125,  -0.0390625}};


float X6 [3][3] = {{-0.0390625, -0.078125,  -0.0390625},
    { 0.0,         0.0,         0.0       },
    { 0.0390625,  0.078125,   0.0390625}};

float X7 [3][3] = {{ 1.0,  0.0, -1.0},
    { 0.0,  0.0,  0.0},
    {-1.0,  0.0,  1.0}};

float X8 [3][3] = {{ 1.0, -2.0,  1.0},
    { 0.0, 0.0,  0.0},
    {-1.0,  2.0, -1.0}};

float X9 [3][3] = {{-0.0390625, -0.078125,  -0.0390625},
    { 0.078125,   0.15625,    0.078125 },
    {-0.0390625, -0.078125,  -0.0390625}};

float X10 [3][3] = {{ 1.0,  0.0, -1.0},
    {-2.0,  0.0,  2.0},
    { 1.0 , 0.0, -1.0}};

float X11 [3][3] = {{ 1.0, -2.0,  1.0},
    {-2.0,  4.0, -2.0},
    { 1.0, -2.0,  1.0}};


cv::Mat L3TL3 = cv::Mat(3,3,CV_32F,X3);
cv::Mat L3TE3 = cv::Mat(3,3,CV_32F,X4);
cv::Mat L3TS3 = cv::Mat(3,3,CV_32F,X5);

cv::Mat E3TL3 = cv::Mat(3,3,CV_32F,X6);
cv::Mat E3TE3 = cv::Mat(3,3,CV_32F,X7);
cv::Mat E3TS3 = cv::Mat(3,3,CV_32F,X8);


cv::Mat S3TL3 = cv::Mat(3,3,CV_32F,X9);
cv::Mat S3TE3 = cv::Mat(3,3,CV_32F,X10);
cv::Mat S3TS3 = cv::Mat(3,3,CV_32F,X11);

cv::Mat filter1;
cv::Mat filter2;
cv::Mat filter3;
cv::Mat filter4;
cv::Mat filter5;
cv::Mat filter6;
cv::Mat filter7;
cv::Mat filter8;
cv::Mat filter9;
cv::Mat filter10;
cv::Mat filter11;
cv::Mat filter12;
cv::Mat filter13;
cv::Mat filter14;
cv::Mat filter15;
cv::Mat filter16;
cv::Mat filter17;

cv::Mat ycbcr;
cv::Mat imageY;
cv::Mat imageCr;
cv::Mat imageCb;
cv::Mat temp;



cv::Mat  applyfilters(cv::Mat image, int M, int N)
{
    cv::Mat out;
    cv::cvtColor(image, ycbcr, cv::COLOR_BGR2YCrCb);
    cv::extractChannel(ycbcr, imageY, 0);
    cv::extractChannel(ycbcr, imageCr, 1);
    cv::extractChannel(ycbcr, imageCb, 2);
    
    conv2(imageY,L3TL3,CONVOLUTION_VALID,temp);
    cv::Size s = temp.size();
    
    filter1 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,L3TE3,CONVOLUTION_VALID,temp);
 
    filter2 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,L3TS3,CONVOLUTION_VALID,temp);
    
    filter3 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,E3TL3,CONVOLUTION_VALID,temp);
    filter4 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,E3TE3,CONVOLUTION_VALID,temp);
    filter5 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,E3TS3,CONVOLUTION_VALID,temp);
    filter6 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,S3TL3,CONVOLUTION_VALID,temp);
    filter7 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,S3TE3,CONVOLUTION_VALID,temp);
    filter8 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageY,S3TS3,CONVOLUTION_VALID,temp);
    filter9 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    
    
    conv2(imageCb,L3TL3,CONVOLUTION_VALID,temp);
    filter10 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    conv2(imageCr,L3TL3,CONVOLUTION_VALID,temp);
    filter11 = temp.colRange(1,s.width-1).rowRange(1, s.height-1);
    
    conv2(imageY,NB1,CONVOLUTION_VALID,filter12);
    conv2(imageY,NB2,CONVOLUTION_VALID,filter13);
    conv2(imageY,NB3,CONVOLUTION_VALID,filter14);
    conv2(imageY,NB4,CONVOLUTION_VALID,filter15);
    conv2(imageY,NB5,CONVOLUTION_VALID,filter16);
    conv2(imageY,NB6,CONVOLUTION_VALID,filter17);

    
    cv::vector<cv::Mat> channels;
    channels.push_back(filter1);
    channels.push_back(filter2);
    channels.push_back(filter3);
    channels.push_back(filter4);
    channels.push_back(filter5);
    channels.push_back(filter6);
    channels.push_back(filter7);
    channels.push_back(filter8);
    channels.push_back(filter9);
    channels.push_back(filter10);
    channels.push_back(filter11);
    channels.push_back(filter12);
    channels.push_back(filter13);
    channels.push_back(filter14);
    channels.push_back(filter15);
    channels.push_back(filter16);
    channels.push_back(filter17);

    //{filter1,filter2,filter3,filter4,filter5,filter6,filter7,filter8,filter9,filter10,filter11,filter12,filter13,filter14,filter15,filter16,filter17};
   
    cv::merge( channels, out);
    
    return out;
    
}

