//
//  filters.hpp
//  Feature
//
//  Created by vishakh duggal on 30/10/15.
//  Copyright © 2015 vishakh duggal. All rights reserved.
//

#ifndef filters_hpp
#define filters_hpp
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdio.h>


cv::Mat applyfilters(cv::Mat, int, int);
#endif /* filters_hpp */
