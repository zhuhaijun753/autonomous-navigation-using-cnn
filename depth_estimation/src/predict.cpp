//
//  predict.cpp
//  Feature
//
//  Created by vishakh duggal on 01/11/15.
//  Copyright © 2015 vishakh duggal. All rights reserved.
//

#include "predict.hpp"

cv::Size s;
cv::Mat labels;
int rows,cols;
int nr_feature = 17;
typedef cv::Vec<float, 17> Vec17d;
static bool first=true;
struct feature_node *x;

struct model* model_0_5_and_5_10=NULL;
struct model* model_0_3_and_3_5=NULL;
struct model* model_0_2_and_2_3=NULL;
struct model* model_0_1_and_1_2=NULL;
struct model* model_1_1_5_and_1_5_2=NULL;
struct model* model_2_2_5_and_2_5_3=NULL;
struct model* model_3_4_and_4_5=NULL;
struct model* model_3_3_5_and_3_5_4=NULL;
struct model* model_4_4_5_and_4_5_5=NULL;
struct model* model_5_8_and_8_10=NULL;
struct model* model_5_7_and_7_8=NULL;
struct model* model_5_6_and_6_7=NULL;
struct model* model_5_5_5_and_5_5_6=NULL;
struct model* model_6_6_5_and_6_5_7=NULL;
struct model* model_7_7_5_and_7_5_8=NULL;
struct model* model_8_9_and_9_10=NULL;
struct model* model_8_8_5_and_8_5_9=NULL;
struct model* model_9_9_5_and_9_5_10=NULL;

cv::Mat predict_lables(cv::Mat features)
{
    
    if(first==true)
    {

        model_0_5_and_5_10 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_0_5_0_and_5_0_10_0.mat");
        model_0_3_and_3_5 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_0_3_0_and_3_0_5_0.mat");
        model_0_2_and_2_3 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_0_2_0_and_2_0_3_0.mat");
        model_0_1_and_1_2 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_0_1_0_and_1_0_2_0.mat");
        model_1_1_5_and_1_5_2 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_1_0_1_5_and_1_5_2_0.mat");
        model_2_2_5_and_2_5_3 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_2_0_2_5_and_2_5_3_0.mat");
        model_3_4_and_4_5 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_3_0_4_0_and_4_0_5_0.mat");
        model_3_3_5_and_3_5_4 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_3_0_3_5_and_3_5_4_0.mat");
        model_4_4_5_and_4_5_5 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_4_0_4_5_and_4_5_5_0.mat");
        model_5_8_and_8_10 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_5_0_8_0_and_8_0_10_0.mat");
        model_5_7_and_7_8 = load_model("//home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_5_0_7_0_and_7_0_8_0.mat");
        model_5_6_and_6_7 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_5_0_6_0_and_6_0_7_0.mat");
        model_5_5_5_and_5_5_6 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_5_0_5_5_and_5_5_6_0.mat");
        model_6_6_5_and_6_5_7 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_6_0_6_5_and_6_5_7_0.mat");
        model_7_7_5_and_7_5_8 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_7_0_7_5_and_7_5_8_0.mat");
        model_8_9_and_9_10 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_8_0_9_0_and_9_0_10_0.mat");
        model_8_8_5_and_8_5_9 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_8_0_8_5_and_8_5_9_0.mat");
        model_9_9_5_and_9_5_10 = load_model("/home/kbipin/catkin_bebop/src/depth_estimation/model_2/model_9_0_9_5_and_9_5_10_0.mat");


        s = features.size();
        rows= s.height;
        cols = s.width;
        x = (struct feature_node *) malloc((nr_feature+2)*sizeof(struct feature_node));
        labels = cv::Mat(cv::Size(cols,rows), CV_8UC1);

        first=false;
    }

    double label;
    int iCount;
    int jCount;

    for( iCount =0;iCount <rows; iCount++)
    {
        for(jCount=0;jCount<cols;jCount++)
        {
            
            x[0].value = fabs(features.at<Vec17d>(iCount,jCount)[0]);
            x[1].value =fabs(features.at<Vec17d>(iCount,jCount)[1]);
            x[2].value =fabs(features.at<Vec17d>(iCount,jCount)[2]);
            x[3].value =fabs(features.at<Vec17d>(iCount,jCount)[3]);
            x[4].value =fabs(features.at<Vec17d>(iCount,jCount)[4]);
            x[5].value =fabs(features.at<Vec17d>(iCount,jCount)[5]);
            x[6].value =fabs(features.at<Vec17d>(iCount,jCount)[6]);
            x[7].value =fabs(features.at<Vec17d>(iCount,jCount)[7]);
            x[8].value =fabs(features.at<Vec17d>(iCount,jCount)[8]);
            x[9].value =fabs(features.at<Vec17d>(iCount,jCount)[9]);
            x[10].value =fabs(features.at<Vec17d>(iCount,jCount)[10]);
            x[11].value =fabs(features.at<Vec17d>(iCount,jCount)[11]);
            x[12].value =fabs(features.at<Vec17d>(iCount,jCount)[12]);
            x[13].value =fabs(features.at<Vec17d>(iCount,jCount)[13]);
            x[14].value =fabs(features.at<Vec17d>(iCount,jCount)[14]);
            x[15].value =fabs(features.at<Vec17d>(iCount,jCount)[15]);
            x[16].value =fabs(features.at<Vec17d>(iCount,jCount)[16]);
            x[0].index =1;
            x[1].index =2;
            x[2].index =3;
            x[3].index =4;
            x[4].index =5;
            x[5].index =6;
            x[6].index =7;
            x[7].index =8;
            x[8].index =9;
            x[9].index =10;
            x[10].index =11;
            x[11].index =12;
            x[12].index =13;
            x[13].index =14;
            x[14].index =15;
            x[15].index =16;
            x[16].index =17;
            x[17].index =-1;
            x[18].index =-1;
            // Level 0-5 and 5-10
            label  = predict(model_0_5_and_5_10,x);
            if (label == -1.0) // 0-5
            {
                // Level 0-3 and 3-5
                label  = predict(model_0_3_and_3_5,x);
                if (label == -1.0) //0-3
                {
                    //Level 0-2 and 2-3
                    label  = predict(model_0_2_and_2_3,x);
                    if (label == -1.0) //0-2
                    {

                        //Level 0-1 and 1-2
                        label  = predict(model_0_1_and_1_2,x);
                        if (label==-1.0) //0-1
                        {
                            labels.at<unsigned char>(iCount,jCount) = 1.0; //0-1
                        }
                        else //1-2
                        {
                            //Level 1-1.5 and 1.2-1.6
                            label  = predict(model_1_1_5_and_1_5_2,x);
                            if(label==-1.0) //1-1.5
                            {
                                labels.at<unsigned char>(iCount,jCount) = 2.0;
                            }
                            else //1.5-2.0
                            {
                                labels.at<unsigned char>(iCount,jCount) = 3.0;
                            }

                        }


                    }
                    else  // 2-3
                    {
                        //Level 2-2.5 and 2.5-3
                        label  = predict(model_2_2_5_and_2_5_3,x);
                        if (label==-1.0) // 2.0-2.5
                        {
                            labels.at<unsigned char>(iCount,jCount) = 4.0;
                        }
                        else //2.5-3.0
                        {
                            labels.at<unsigned char>(iCount,jCount) = 5.0;
                        }
                        
                    }
                    

                }
                else // 3-5
                {
                    //Level 3.0-4.0 and 4.0-5.0
                    label  = predict(model_3_4_and_4_5,x);
                    if (label==-1.0) // 3.0-4.0
                    {
                        //Level 3.0-3.5 and 3.5-4.0
                        label  = predict(model_3_3_5_and_3_5_4,x);
                        if (label ==-1.0) // 3.0-3.5
                        {
                            labels.at<unsigned char>(iCount,jCount) = 6.0;
                        }
                        else //3.5-4.0
                        {
                            labels.at<unsigned char>(iCount,jCount) = 7.0;
                        }
                        
                    }
                    else //4.0-5.0
                    {
                        //Level 4.0-4.5 and 4.5-5.0
                        label  = predict(model_4_4_5_and_4_5_5,x);
                        if (label ==-1.0) //4.0-4.5
                            labels.at<unsigned char>(iCount,jCount) = 8.0;
                        else //4.5-5.0
                            labels.at<unsigned char>(iCount,jCount) = 9.0;
                        
                    }

                }

            }
            else // 5-10
            {
                //Level 5.0-8.0 and 8.0-10.0
                label  = predict(model_5_8_and_8_10,x);
                if (label ==-1.0) //5.0-8.0
                {
                    //Level 5.0-7.0 and 7.0-8.0
                    label  = predict(model_5_7_and_7_8,x);
                    if (label ==-1.0) //5.0-7.0
                    {
                        //Level 5.0-6.0 and 6.0-7.0
                        label  = predict(model_5_6_and_6_7,x);
                        if (label ==-1.0) //5.0-6.0
                        {
                            //Level 5.0-5.5 and 5.5-6.0
                            label  = predict(model_5_5_5_and_5_5_6,x);
                            if (label ==-1.0) //5.0-5.5
                                labels.at<unsigned char>(iCount,jCount) = 10.0;
                            else //5.5-6.0
                                labels.at<unsigned char>(iCount,jCount) = 11.0;

                        }
                        else  // 6.0-7.0
                        {
                            //Level 6.0-6.5 and 6.5-7.0
                            label  = predict(model_5_6_and_6_7,x);
                            if (label ==-1.0) //5.0-6.0
                            {
                                //Level 5.0-5.5 and 5.5-6.0
                                label  = predict(model_6_6_5_and_6_5_7,x);
                                if (label ==-1.0) //6.0-6.5
                                    labels.at<unsigned char>(iCount,jCount) = 12.0;
                                else //6.5-7.0
                                    labels.at<unsigned char>(iCount,jCount) = 13.0;

                            }


                        }

                    }
                    else //7.0-8.0
                    {
                        //Level 7.0-7.5 and 7.5-8.0
                        label  = predict(model_7_7_5_and_7_5_8,x);
                        if (label ==-1.0) //7.0-7.5
                            labels.at<unsigned char>(iCount,jCount) = 14.0;
                        else //7.5-8.0
                            labels.at<unsigned char>(iCount,jCount) = 15.0;

                    }

                }
                else //8.0-10.0
                {
                    //Level 8.0-9.0 and 9.0-10.0
                    label  = predict(model_8_9_and_9_10,x);
                    if (label ==-1.0) //8.0-9.0
                    {
                        //Level 8.0-8.5 and 8.5-9.0
                        label  = predict(model_8_8_5_and_8_5_9,x);
                        if (label==-1.0) //8.0-8.5
                            labels.at<unsigned char>(iCount,jCount) = 16.0;
                        else //8.5-9.0
                            labels.at<unsigned char>(iCount,jCount) = 17.0;

                    }
                    else //9.0-10.0
                    {
                        label  = predict(model_9_9_5_and_9_5_10,x);
                        if (label ==-1.0) //9.0-9.5
                            labels.at<unsigned char>(iCount,jCount) = 18.0;
                        else //9.5-10.0
                            labels.at<unsigned char>(iCount,jCount) = 19.0;

                    }


                }
                
            }
            
        }
        
    }
    return labels;
}
