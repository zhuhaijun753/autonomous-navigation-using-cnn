/****************************************************************************
* Software License Agreement (Apache License)
*
*     Copyright (C) 2012-2013 Open Source Robotics Foundation
*
*     Licensed under the Apache License, Version 2.0 (the "License");
*     you may not use this file except in compliance with the License.
*     You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
*     Unless required by applicable law or agreed to in writing, software
*     distributed under the License is distributed on an "AS IS" BASIS,
*     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*     See the License for the specific language governing permissions and
*     limitations under the License.
*
*****************************************************************************/

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <time.h>
#include <fstream>

#include <ros/ros.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <camera_calibration_parsers/parse.h>

#define ARDRONE_RAW

using namespace cv;
using namespace std;

cv::VideoWriter outputVideo;

int g_count = 0;
std::string encoding;
std::string codec;
int fps;
std::string filename;

void callback(const sensor_msgs::ImageConstPtr& image_msg, const sensor_msgs::CameraInfoConstPtr& info)
{
    cv::Mat image, img;
    double min, max;

    try
    {
        image = cv_bridge::toCvShare(image_msg, encoding)->image;
    } catch(cv_bridge::Exception)
    {
        ROS_ERROR("Unable to convert %s image to %s", image_msg->encoding.c_str(), encoding.c_str());
        return;
    }

    minMaxLoc(image, &min, &max);
    image.convertTo(img, CV_32FC1);
    img = ((image / max) * 255);
    cv::namedWindow("Display window",CV_WINDOW_AUTOSIZE);
    cv::imshow("Display window", img);
    waitKey(1);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "video_viewer", ros::init_options::AnonymousName);
    ros::NodeHandle nh;

    image_transport::ImageTransport it(nh);

    std::string topic = nh.resolveName("/depth_estimation/image");
    image_transport::CameraSubscriber sub_camera = it.subscribeCamera(topic, 1, &callback);

    image_transport::Subscriber sub_image = it.subscribe("/depth_estimation/image", 1,
                                                         boost::bind(callback, _1, sensor_msgs::CameraInfoConstPtr()));

    ROS_INFO_STREAM("Waiting for topic " << "/depth_estimation/image" << "...");
    ros::spin();
}
