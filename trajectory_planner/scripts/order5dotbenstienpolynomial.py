__author__ = 'kripalu'
from cvxopt import matrix
#B = matrix(0, (1, 6))

def order5dotbenstienpolynomial(x):
	B = [];
	B.append(-5*(1-x)**4)
	B.append(5 * ((1-x)**4 - 4 * x * (1-x)**3))
	B.append(10 * (2 * x * (1-x)**3 - 3*x**2 * (1-x)**2))
	B.append(10 * (3 * x**2 * (1-x)**2 - 2 * x**3 * (1-x)))
	B.append(5 * (4 * x**3 * (1-x) - x**4))
	B.append(5 * x**4)
	return  B
    #B[0] = -5*(1-x)**4
    #B[1] = 5 * ((1-x)**4 - 4 * x * (1-x)**3)
    #B[2] = 10 * (2 * x * (1-x)**3 - 3*x**2 * (1-x)**2)
    #B[3] = 10 * (3 * x**2 * (1-x)**2 - 2 * x**3 * (1-x))
    #B[4] = 5 * (4 * x**3 * (1-x) - x**4)
    #B[5] = 5 * x**4
    