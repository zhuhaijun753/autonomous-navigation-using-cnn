from math import factorial
from numpy import array
def rotate(l,n):
    return l[-n:] + l[:-n]

def Bspline_basis(i , u , p , U):
	if u == U[17]:
		B = [0,0,0,0,0,0,0,0,0,0,0,1]
		return B
	elif u == U[0]:
		B = [1,0,0,0,0,0,0,0,0,0,0,0]
		return B
	else:	
		DR = [0]*12
		DL = [0]*12
		B = [0]*12
		B[0] = 1 
		
		for j in range(1,(p+1)):
			DL[j] = u - U[i+1-j]
			DR[j] = U[i+j] - u
			acc = 0.0
			for r in range(0,j):
				temp = B[r]/(DR[r+1] + DL[j-r])
				B[r] = acc + DR[r+1]*temp
				acc = DL[j-r]*temp

			B[j] = acc
		if i > 5:
			return rotate(B,i-5)
		else:
			return B

def WhichSpan(u , U , n_knot , p):
	high = n_knot - p;
	low = p;
	if u == U[high]:
		mid = high
	else:
		mid = (high + low) / 2
		while ((u < U[mid]) or (u >=U[mid+1])):
			if u == U[mid+1]:
				mid = mid +1
			else:
				if u > U[mid]:
					low = mid
				else:
					high = mid

				mid = (high + low) / 2

	return mid

def a(k,i,j,p,t):
	
	if( k ==0 and i ==0) :# i == k ==0
	    f = 1
	    
	elif (i == k) :# i == k
	    
	    if (t[j+p+1] - t[j+k]) ==0:
               f=0
	    else:
	       f = -a(k-1,k-1,j,p,t)/(t[j+p+1] - t[j+k])
	elif ( k !=0 and i == 0) :# i == 0
	    if ( t[ j + p - k + 1] - t[j]) ==0:
	       f = 0;
	    else:
	       f = a(k-1,0,j,p,t) / ( t[ j + p - k + 1] - t[j])
	else:
	    if (t [j+p+i-k+1] - t[j+i]) == 0:
			f = 0
	    else:
	        f = (a (k-1,i,j,p,t) - a(k-1,i-1,j,p,t)) / (t [j+p+i-k+1] - t[j+i])
	
	if  f > 999 or f < -999:
	    #%disp('making A zero ');
	    f = 0
	return f
	


def BSplineDrev(n,t,x,k):
	# n = order of polynomial p + 1
	# t = knot vector
	# x = point at which we need to calculate 
	#k = 1,2,3 ... differential degree
	p = n-1
	B = [0]*12
	for j in range(0, len(t)-n):
	    sumofval = 0
	    for i in range(0,k+1):
	    	#print   a(k,i,j,p,t)
	    	#print   bspline_basis (j+i,n-k,t,x)
	        sumofval = sumofval + ( a(k,i,j,p,t) * bspline_basis (j+i,n-k,t,x))
	    sumofval = sumofval * (factorial(p) / factorial( p - k))
	    B[j] = sumofval
	return B



def bspline_basis(j,n,t,x):

	return bspline_basis_recurrence(j,n,t,x)

def bspline_basis_recurrence(j,n,t,x):
	y = 0
	#y = zeros(size(x));
	if n > 1:
	    b = bspline_basis(j,n-1,t,x)
	    dn = x - t[j]
	    dd = t[j+n-1] - t[j]
	    if dd != 0:  
	        y = y + b *( dn / dd)
	    
	    b = bspline_basis(j+1,n-1,t,x)
	    dn = t[j+n] - x
	    dd = t[j+n] - t[j+1]
	    if dd != 0:
	        y = y + b *( dn / dd)
	    
	elif t[j+1] < t[-1]:
		if t[j] <= x and x < t[j+1]:
			y = 1
	else:
		if t[j] <= x:
			y = 1;
	
	return y




		
	

#def main():
	#u = [0,0,0,0,0,0,0.1428,0.2856, 0.4284,0.5712,0.7140, 0.8568,1,1,1,1,1,1]
	#u = [ 0 , 0 , 0 , 0 , 1 , 2 , 4 ,7 ,7,7,7]
	#i = WhichSpan(0.5,u,17,5)
	#print(i)
	#B = Bspline_basis(i, 0.8568, 5, u)
	#print(B)
	#BSplineDrev(5,u,0.8568,1)
	#A = basis_drev(1,i,5,3,u)
	#n =6
	
	#A = BSplineDrev(n,u,0,2)
	#print A
#main()


