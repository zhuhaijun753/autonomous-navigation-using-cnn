__author__ = 'kripalu'
import sys
sys.path.append("/usr/lib/python2.7/dist-packages/numpy")
#import numpy as np
import time
from numpy.linalg import pinv
from numpy import dot
from cvxopt import matrix

basis_at_gridpoints = matrix([1,0,0,0,0,0,0,0,0,0,0,0,0,0.0625000000000000,0.405092592592593,0.410879629629630,0.113194444444444,0.00833333333333334,0,0,0,0,0,0,0,0,0.0185185185185185,0.259259259259259,0.497222222222222,0.216666666666667,0.00833333333333334,0,0,0,0,0,0,0,0,0.0104166666666667,0.214583333333333,0.550000000000000,0.216671332586786,0.00832866741321389,0,0,0,0,0,0,0,0,0.00833333333333332,0.216666666666667,0.550120151524532,0.214477755667342,0.0104020928081268,0,0,0,0,0,0,0,0,0.00833333333333335,0.216945168156792,0.497306484124311,0.258948271750867,0.0184667426346959,0,0,0,0,0,0,0,0,0.00839687322046854,0.113706555427425,0.411437064707589,0.404308424145673,0.0621510824988448,0,0,0,0,0,0,0,0,0,0,0,0,1],(12,8))

# Get Basis 1st differential at Uo
dotB_uo =  matrix([-35.0140056022409,35.0140056022409,0,0,0,0,0,0,0,0,0,0],(1,12))
  #dotB_uo = BSplineDifferential(n-1,t,u(1)),
# Get Basis 1st differential at Uf
dotB_uf =  matrix([0,0,0,0,0,0,0,0,0,0,-34.9162011173184,34.9162011173184],(1,12))
  #dotB_uf = BSplineDifferential(n-1,t,u(numel(u))),
# Get Basis 2nd differential at Uo
ddotB_uo = matrix([980.784470651005,-1471.17670597651,490.392235325503,0,0,0,0,0,0,0,0,0],(1,12))
  #ddotB_uo = BSplineDDifferential(n-2,t,u(1)),
# Get Basis 2nd differential at Uf
ddotB_uf = matrix([0,0,0,0,0,0,0,0,0,488.338477165293,-1463.65135753732,975.312880372023],(1,12)) 
  #ddotB_uf = BSplineDDifferential(n-2,t,u(numel(u))),
M = 8
N = 12

def BsplineControlPoints(n,t,u,q,vo,vf,ao,af):
    """ Bspline """
    A = matrix([basis_at_gridpoints.T,dotB_uo,dotB_uf,ddotB_uo,ddotB_uf]) 
    q_new = list(q)  
    q_new.append(vo)
    q_new.append(vf)
    q_new.append(ao)
    q_new.append(af)
    c = matrix([q_new],(12,1))	
    p = dot(pinv(A),c)
    p = [l[0] for l in p.tolist()]	
    return p
