from numpy.linalg import pinv
from numpy import dot,concatenate,vstack
from cvxopt import matrix , spmatrix 
from numpy import sum , array,transpose
from cvxpy import *


def Constraints_Next(X,new_u,b,dot_bsplien,ddot_bsplien,dddot_bsplien,maxconstraints,endconstraints): 

	vo = endconstraints[0];
	vf = endconstraints[1];
	ao = endconstraints[2];
	af = endconstraints[3];
	vmax = maxconstraints[0]; 
	amax = maxconstraints[1];
	jmax = maxconstraints[2];

	Ain = matrix(dot_bsplien +  ddot_bsplien + dddot_bsplien,(12,24)).T	
#	print(Ain)
	Bin = matrix([ vmax, vmax, vmax, vmax, vmax, vmax, vmax, vmax, 
        amax, amax, amax, amax, amax, amax, amax, amax,
        jmax, jmax, jmax, jmax, jmax, jmax, jmax, jmax],(24,1));
#	print(Bin)
	Aeq = matrix([dot_bsplien[0], dot_bsplien[-1], ddot_bsplien[0], ddot_bsplien[-1]],(12,4)).T;
	Beq = matrix([vo,  vf,  ao,  af],(4,1)) 
#	print(Aeq)
#	print(Beq)
	A = matrix(b,(12,8)).T;
	B = matrix(X);
	#print(A.size)
	#print(B.size)
	#print A
	#print B	
	x = Variable(12)
        objective = Minimize(norm(A*x - B))
        constraints = [Ain*x <= Bin,Aeq*x==Beq]
        #constraints = [Aeq*x==Beq]
        prob = Problem(objective, constraints)

        # The optimal objective is returned by prob.solve().
        result = prob.solve()
        # The optimal value for x is stored in x.value.
#        print result
	#print x.value
        #p = [l[0] for l in x.value.tolist()]
        return x.value;
