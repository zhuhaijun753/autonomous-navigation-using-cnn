from numpy.linalg import pinv
from numpy import dot
from cvxopt import matrix , spmatrix 
from numpy import sum , array,transpose
from cvxpy import *


def Constraints(u,px,py,pz,dot_b,ddot_b,dddot_b,maxconstraints,endconstraits):

	vox = endconstraits[0]
	voy = endconstraits[1]
	voz = endconstraits[2]
	vfx = endconstraits[3]
	vfy = endconstraits[4]
	vfz = endconstraits[5]
	aox = endconstraits[6]
	aoy = endconstraits[7]
	aoz = endconstraits[8]
	afx = endconstraits[9]
	afy = endconstraits[10]
	afz = endconstraits[11]
	vmax = maxconstraints[0] 
	amax = maxconstraints[1]
	jmax = maxconstraints[2]
	#% Calculate the constraints
	#% Inequality constraints velocity , cceleration and Jerk

	vx = dot(dot_b,px)**2 #[i ** 2 for i in dot(dot_b,px)]#[[y**2 for y in x] for x in dot(dot_b,px)]
	vy = dot(dot_b,py)**2#[[y**2 for y in x] for x in dot(dot_b,py)]
	vz = dot(dot_b,pz)**2#[[y**2 for y in x] for x in dot(dot_b,pz)]
	ax = dot(ddot_b,px)
	ay = dot(ddot_b,py)
	az = dot(ddot_b,pz)
	jx = pow(dot(dddot_b,px)**2,0.3333) #[[pow(y**2,0.3333) for y in x] for x in dot(dddot_b,px)] #dot(dddot_b,px)
	jy = pow(dot(dddot_b,py)**2,0.3333) #[[pow(y**2,0.3333) for y in x] for x in dot(dddot_b,py)]
	jz = pow(dot(dddot_b,pz)**2,0.3333) #[[pow(y**2,0.3333) for y in x] for x in dot(dddot_b,pz)]
	rows = range(0,72)
	columns = range(0,8)*9
	parameter = vx.tolist() + vy.tolist()  + vz.tolist() + ax.tolist()  + ay.tolist()  + az.tolist()  + jx.tolist()  + jy.tolist()  + jz.tolist()
	Ain = spmatrix(parameter,rows,columns)
	#for i=1:numel(u)   
	 #Ain(i,i) = sum(dot_b(i,:) .* px)^2
	 #Ain(i +1*numel(u),i) = sum(dot_b(i,:) .* py)^2
	 #Ain(i +2*numel(u),i) = sum(dot_b(i,:) .* pz)^2
	 #Ain(i+ 3*numel(u),i) = sum(ddot_b(i,:) .* px)
	 #Ain(i+ 4*numel(u),i) = sum(ddot_b(i,:) .* py) 
	 #Ain(i+ 5*numel(u),i) = sum(ddot_b(i,:) .* pz)
	 #Ain(i+ 6*numel(u),i) = nthroot(sum(dddot_b(i,:) .* px),3)^2
	 #Ain(i+ 7*numel(u),i) = nthroot(sum(dddot_b(i,:) .* py),3)^2
	 #Ain(i+ 8*numel(u),i) = nthroot(sum(dddot_b(i,:) .* pz),3)^2
	#end

	# Now we have Inequality Constraints the Ax <= B ..... -->  B matrix
	squared_vmax = vmax
	squared_cuberoot_jmax = pow(jmax**2,0.333333)

	#% WE are using 8 points between the end and start point ...
	Bin = matrix([  squared_vmax**2, squared_vmax**2, squared_vmax**2, squared_vmax**2, \
		        squared_vmax**2, squared_vmax**2, squared_vmax**2, squared_vmax**2,  \
		        squared_vmax**2, squared_vmax**2, squared_vmax**2, squared_vmax**2, \
		        squared_vmax**2, squared_vmax**2, squared_vmax**2, squared_vmax**2,  \
		        squared_vmax**2, squared_vmax**2, squared_vmax**2, squared_vmax**2, \
		        squared_vmax**2, squared_vmax**2, squared_vmax**2, squared_vmax**2,  \
			amax, amax, amax, amax,  amax, amax, amax, amax,  \
			amax, amax, amax, amax,  amax, amax, amax, amax,  \
			amax, amax, amax, amax, amax, amax, amax, amax,  \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax, \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax, \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax, \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax, \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax, \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax, \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax, \
			squared_cuberoot_jmax, squared_cuberoot_jmax, squared_cuberoot_jmax],(72,1))




	squared_velocity = vx + vy + vz
	A = spmatrix(squared_velocity.tolist(),range(0,8),range(0,8))
	b = matrix([3*squared_vmax**2,3*squared_vmax**2,3*squared_vmax**2,3*squared_vmax**2, \
	     3*squared_vmax**2,3*squared_vmax**2,3*squared_vmax**2,3*squared_vmax**2],(8,1))


	x = Variable(8)
	objective = Minimize(norm(A*x - b))
	constraints = [Ain*x <= Bin]
	prob = Problem(objective, constraints)

	# The optimal objective is returned by prob.solve().
	result = prob.solve()
	# The optimal value for x is stored in x.value.
#	print x.value
	p = [l[0] for l in x.value.tolist()]
	return p
