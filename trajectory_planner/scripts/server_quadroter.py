#!/usr/bin/env python
from trajectory_planner.srv import *
import rospy

from cvxopt import matrix, sqrt
from math import cos, sin
import os
from datetime import datetime
from Controller import Controller

def handle_trajectoryPlanner(req) :

        roll, pitch, gaz = Controller(req.xo, req.xf, req.xdoto, req.xdotf, req.xddoto, req.xddotf, req.yo, req.yf, req.ydoto, req.ydotf, req.yddoto, req.yddotf, req.zo, req.zf, req.zdoto, req.zdotf, req.zddoto, req.zddotf)

        yaw = 0.0

	print (roll, pitch, yaw, gaz)

	return(roll, pitch, yaw, gaz)

if __name__ == "__main__":
	global datafile
        pwd = os.getcwd()
	rospy.init_node('trajectory_planner_server')
    	s = rospy.Service('trajectoryPlanner', TrajectoryPlanner, handle_trajectoryPlanner)
    	print "Trajectory Planer Server Running..."
    	rospy.spin()

