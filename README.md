# README #

This is a ROS based framework for Autonomous Navigation of Quadcopter using Convolutional Neural networks and utilises Lasagne and Theano based implementation. This framework is developed for Parrot Bebop Drone over ROS middleware.

### What is this repository for? ###

Developing framework for Autonomous Navigation of quadcopter.

### How do I get set up? ###

Download the packages and launch over ROS ( Indigo ) with Ubuntu 14.04.  



### Who do I talk to? ###
* Admin : vishakh Duggal Email : vishakh.duggal@gmail.com