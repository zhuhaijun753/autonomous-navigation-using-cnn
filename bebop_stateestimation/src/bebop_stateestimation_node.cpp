/*******************************************************************************
   Kumar Bipin
   kumar.bipin@research.iiit.ac.in

International Institute of Information Technology, Hyderabad

This code is developed to test the control command to Ardrone 2.0
consequently which are use for System Identification Purpose

*********************************************************************************/

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sensor_msgs/Image.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Empty.h>
#include <std_srvs/Empty.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <camera_info_manager/camera_info_manager.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <fstream>
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "bebop_autonomy/NavdataGPS.h"
#include "bebop_autonomy/NavdataVelocity.h"
#include "bebop_stateestimation.h"

#define pi 3.14159265358979323846

using namespace cv;
using namespace std;

struct Bebop_State state;

struct Attitude
{
    float roll;
    float pitch;
    float yaw;
} Atd;

struct Velocity Vt;
struct Acceleration At;
struct Position Xt;

void velocity_callback(const bebop_autonomy::NavdataVelocity &msg_in)
{
    Vt.vx=msg_in.Vx;
    Vt.vy=msg_in.Vy;
    Vt.vz=msg_in.Vz;
}
void attitude_callback(const bebop_autonomy::NavdataAttitude &msg_in)
{
    state.roll=msg_in.roll;
    state.pitch=msg_in.pitch;
    state.yaw=msg_in.yaw;
}

int main(int argc, char** argv)
{

    double dt=0.2;

    ros::init(argc, argv,"bebop_stateestimation");
    ros::NodeHandle nh_;
    ros::Rate loop_rate(50);
    ros::Subscriber velocity_sub;
    ros::Subscriber attitude_sub;


    /*********************** Subscribe ************** */

    velocity_sub = nh_.subscribe("/bebop/velocity", 1, velocity_callback);
    attitude_sub = nh_.subscribe("/bebop/attitude", 1, attitude_callback);



    while (ros::ok())
    {

        Xt=bebop_estimate_position(Vt.vx, Vt.vy, Vt.vz, dt);
        At=bebop_estimate_acceleration(Vt.vx, Vt.vy, Vt.vz, dt);

        ros::spinOnce();
        loop_rate.sleep();
    }/*ros::ok*/

    ROS_ERROR("ROS::OK Node Closing");

    ros::shutdown();
}//main
