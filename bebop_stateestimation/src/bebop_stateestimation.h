/********************************************************************************
Kumar Bipin
Robotics Research Lab@

International Institute of Information Technology, Hyderabad

This code is developed to test the control command to Ardrone 2.0
consequently which are use for System Identification Purpose
*********************************************************************************/
#ifndef _BEBOP_STATEESTIMATION_H_
#define _BEBOP_STATEESTIMATION_H_

#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "bebop_autonomy/NavdataGPS.h"
#include "bebop_autonomy/NavdataVelocity.h"
#include "bebop_autonomy/NavdataAttitude.h"

struct Bebop_State {

    float roll;
    float pitch;  
    float yaw; 
    float longitude;
    float latitude;
    float altitude;
    float x; 
    float y; 
    float z; 
    float vx; 
    float vy; 
    float vz; 
    float ax; 
    float ay; 
    float az; 
};

struct Position {
   float x;
   float y;
   float z;
};

struct Velocity {
   float vx;
   float vy;
   float vz;
};

struct Acceleration {
  float ax;
  float ay;
  float az;
};

struct Position bebop_estimate_position(float vx, float vy, float vz, double tk);
struct Acceleration bebop_estimate_acceleration(float vx, float vy, float vz, double tk);
struct Position bebop_position_kalman_fusion(struct Position Ximu, struct Position Xgps);

#endif
