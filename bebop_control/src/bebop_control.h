/********************************************************************************
   Kumar Bipin
   kumarb@uurmi.com

Uurmi System Lab, Hyderabad

This code is developed to test the control command to Ardrone 2.0
consequently which are use for System Identification Purpose
*********************************************************************************/
#ifndef _BEBOP_CONTROL_H_
#define _BEBOP_CONTROL_H_

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Vector3.h>
#include <std_srvs/Empty.h>
#include <std_msgs/Empty.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/String.h>

#include "bebop_autonomy/NavdataAttitude.h"
#include "bebop_autonomy/NavdataGPS.h"
#include "bebop_autonomy/NavdataVelocity.h"
#include "bebop_autonomy/dataCamera.h"
#include "bebop_autonomy/dataPCMD.h"

struct joystick {
    int x;
    int y;
    int z;
    int yaw;
    int takeoff;
    int land;
    int emergency;
    int exp;
};

struct command
{
  float pitch;
  float roll;
  float yaw;
  float gaz;
};

struct Attitude
{
    float roll;
    float pitch;
    float yaw;
};

struct GPSPosition
{
    double lat;
    double lng;
    double hgt;

};

struct ORBPosition
{
    double x;
    double y;
    double z;

};

void navDataGPSCallback(const bebop_autonomy::NavdataGPS &msg);
void navDataVelocityCallback(const bebop_autonomy::NavdataVelocity &msg);
void joy_callback(const sensor_msgs::Joy& joy_msg_in);

#endif
