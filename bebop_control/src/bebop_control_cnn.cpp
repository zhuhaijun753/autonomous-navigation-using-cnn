/*******************************************************************************
   Kumar Bipin
   kumar.bipin@research.iiit.ac.in

Uurmi System Lab, Hyderabad

This code is developed to test the control command to Ardrone 2.0
consequently which are use for System Identification Purpose
*********************************************************************************/

#include "bebop_control.h"

using namespace std;
using namespace cv;

#define  MAXLOOP 1000

#define HOVER     0
#define GOFORWARD 1
#define MOVELEFT  2
#define MOVERIGHT 3
#define EXPLORE   4

ros::Publisher takeoff_pub;
ros::Publisher landing_pub;
ros::Publisher cmd_pub;

bool firstRunExp=false;

static struct joystick joy;
struct Attitude atd;

unsigned int simplecmd;
unsigned int counter=0;


void joy_callback(const sensor_msgs::Joy& joy_msg_in)
{
    //Take in PS3 xbox controller

    joy.x=joy_msg_in.axes[5];                     //left stick up-down
    joy.y=joy_msg_in.axes[4];                     //left stick left-right

    joy.z=joy_msg_in.buttons[6];                  //Left Bottom  button
    if(!joy.z) joy.z=-joy_msg_in.buttons[7];      //right Bottom  button

    joy.yaw=joy_msg_in.buttons[4];                //Left Upper
    if(!joy.yaw) joy.yaw=-joy_msg_in.buttons[5];  //Right Lower

    joy.takeoff=joy_msg_in.buttons[3];            //Y button
    joy.land=joy_msg_in.buttons[0];                      //X button
    joy.emergency=joy_msg_in.buttons[2];              // button

    joy.exp=joy_msg_in.buttons[1];                //Cross button
}

void flightPlanningCallback(const std_msgs::String::ConstPtr& msg)
{

   if(strcmp(msg->data.c_str(), "GoStraight") == 0)
     {
         simplecmd=GOFORWARD;
     }
   else if(strcmp(msg->data.c_str(), "MoveLeft") == 0)
     {
         simplecmd=MOVELEFT;
     }
   else if(strcmp(msg->data.c_str(), "MoveRight") == 0)
     {
        simplecmd=MOVERIGHT;
     }
   else if(strcmp(msg->data.c_str(), "Explore") == 0)
     {
        simplecmd=EXPLORE;
     }
   else if(strcmp(msg->data.c_str(), "Hover") == 0)
     {
       simplecmd=HOVER;
     }
     //ROS_INFO("I heard: [%s], %d", msg->data.c_str(), simplecmd);
}

void wait_for_time(unsigned int wait_time)
{
    unsigned long i=0, millisecond;
    clock_t start_t,end_t;
    start_t = clock();
    while(1)
    {
        end_t = clock();
        millisecond = (end_t - start_t) * 1000 / CLOCKS_PER_SEC;
        if (millisecond > wait_time)
        {
            break;
        }
        for(i=0;i<0xFF;i++);
    }
}

void send_control_cmd(int flag, int roll , int pitch, int yaw, int gaz)
{

    bebop_autonomy::dataPCMD cmd;
    
    cmd.flag = flag;
    cmd.roll = roll;
    cmd.pitch = pitch;
    cmd.yaw =  yaw;
    cmd.gaz =  gaz;

    cmd_pub.publish(cmd);
    if( !((pitch==0.0) && (roll==0.0) && (gaz == 0.0) && (yaw == 0.0)))
        wait_for_time(200);

}

void autonomous_flight_cruise()
{

     if(simplecmd==GOFORWARD)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(1, 0, 12, 0, 0);
      }
      else if(simplecmd==MOVELEFT)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(1, -3, 0, 0, 0);
      }
      else if(simplecmd==MOVERIGHT)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(1, 3, 0, 0, 0);
      }
      else if(simplecmd==EXPLORE)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(1, 0, 0, 60, 0);
      }
      else if(simplecmd==HOVER)
      {
        ROS_INFO("I heard:%d", simplecmd);
        send_control_cmd(0, 0, 0, 0, 0);
      }

}
int main(int argc, char** argv)
{

    unsigned int looprate=50;
    bool takeoff=false;

    ros::init(argc, argv,"Bebop_fly_test");
    ros::NodeHandle nh_;
    ros::Rate loop_rate(looprate);
    ros::Subscriber joy_sub;
    ros::Subscriber flightPlanning_sub;

    /*********************** Subscribe ************** */

    joy_sub = nh_.subscribe("/joy", 1, joy_callback);
    flightPlanning_sub = nh_.subscribe("/flightPlanning/cmd", 1, flightPlanningCallback);

    /*********************** Advertise ************** */
    std::string takeoff_channel = nh_.resolveName("/bebop/takeoff");
    takeoff_pub  = nh_.advertise<std_msgs::Empty>(takeoff_channel,1);

    std::string landing_channel = nh_.resolveName("/bebop/landing");
    landing_pub  = nh_.advertise<std_msgs::Empty>(landing_channel,1);

    std::string control_channel = nh_.resolveName("/bebop/cmdvel");
    cmd_pub = nh_.advertise<bebop_autonomy::dataPCMD>(control_channel,1);

    double time =(double)ros::Time::now().toSec();
    while((double)ros::Time::now().toSec() < time+3);

    while (ros::ok())
    {

         //autonomous_flight_cruise();
        /*commands to change state of drone*/
        if(joy.takeoff)
        {
            ROS_INFO("#...TAKEOFF...#");
            takeoff_pub.publish(std_msgs::Empty());
            firstRunExp=false;

            memset(&joy, 0, sizeof(struct joystick));
        }
        if(joy.land)
        {
            ROS_INFO("#...LANDING...#");
            landing_pub.publish(std_msgs::Empty());
            firstRunExp=false;

            memset(&joy, 0, sizeof(struct joystick));
        }

        if(joy.emergency)
        {
            ROS_INFO("#...LANDING...#");
            landing_pub.publish(std_msgs::Empty());
            //emergency_pub.publish(std_msgs::Empty());
            firstRunExp=false;

            memset(&joy, 0, sizeof(struct joystick));
        }
        if(joy.exp)
        {
            if(!firstRunExp)
            {
                ROS_INFO("#...EXPERIMENT STARTED...#");
                firstRunExp=true;
            }
            memset(&joy, 0, sizeof(struct joystick));
        }
        if(abs(joy.x))
        {
            if(joy.x > 0)
            {
                ROS_INFO("#...FORWARD...#");
                send_control_cmd(1, 0, 12, 0, 0);
            }
            else
            {
                ROS_INFO("#...BACKWARD...#");
                send_control_cmd(1, 0, -25, 0, 0);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(abs(joy.y))
        {
            if(joy.y > 0)
            {
                ROS_INFO("#...LEFT...#");
                send_control_cmd(1, -10, 0, 0, 0);
            }
            else
            {
                ROS_INFO("#...RIGHT...#");
                send_control_cmd(1, 10, 0, 0, 0);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(abs(joy.z))
        {
            if(joy.z > 0)
            {
                ROS_INFO("#...DOWN...#");
                send_control_cmd(0, 0, 0, 0, -50);
            }
            else
            {
                ROS_INFO("#...UP...#");
                send_control_cmd(0, 0, 0, 0, 50);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(abs(joy.yaw))
        {

            if(joy.yaw > 0)
            {
                ROS_INFO("#...ANTI-CLOCKWISE...#");
                send_control_cmd(0, 0, 0, -20, 0);
            }
            else
            {
                ROS_INFO("#...CLOCKWISE...#");
                send_control_cmd(0, 0, 0, 20, 0);
            }
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
        else if(firstRunExp)
        {
            /***************************************************************************/
            if(counter <= MAXLOOP)
            {
                 ROS_INFO("#... Autonomous Flight ... .. .");
                 autonomous_flight_cruise();
                 
            }
            else
            {
                firstRunExp = false;
                ROS_INFO("#... Manual Flight ... .. .");
                goto done;
            }
            counter++;
            /**************************************************************************/
        }
        else
        {
            //ROS_INFO("#...HOVER...#");
            send_control_cmd(0, 0, 0, 0, 0);
            
            firstRunExp=false;
            memset(&joy, 0, sizeof(struct joystick));
        }
done:
        ros::spinOnce();
        loop_rate.sleep();
    }/*ros::ok*/

    ROS_ERROR("ROS::OK failed- Node Closing");
    ros::shutdown();

}//main
