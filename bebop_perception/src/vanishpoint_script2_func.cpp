
/* Function to detect vanishing point */

/* Author : V. Sai Sindhu */

#include "vanishpoint_script2_func.hpp"
#include <math.h> 
#include <stdlib.h>

extern "C" {
#include "lsd.h"
}

using namespace std;
using namespace cv;

Point2f lineintersect(Vec4i line1,Vec4i line2)
{
Point2f pt;
int a1,b1,a2,b2,c1,c2;
int x1,y1,x2,y2,x3,y3,x4,y4;
x1=line1[0];
y1=line1[1];
x2=line1[2];
y2=line1[3];
x3=line2[0];
y3=line2[1];
x4=line2[2];
y4=line2[3];
a1=y2-y1;
a2=y4-y3;
b1=x1-x2;
b2=x3-x4;
c1=x2*y1-x1*y2;
c2=x4*y3-x3*y4;
double det=a1*b2-a2*b1;
if(det==0)
{
pt.x=-1;
pt.y=-1;
return pt;
}
else
{
pt.x=(b1*c2 - b2*c1)/det;
pt.y=(a2*c1 - a1*c2)/det;
return pt;
}
}
vector<Vec4i> linesupport(Point3f vp,vector<Vec4i> lines,vector<double>theta,Mat Img)
{
  vector<Vec4i> lines1;
for(int i=0 ;i<lines.size();i++)
{
  Vec4i l;
  l=lines[i];
  int x,y;
  x=(l[0]+l[2])/2 - vp.x;
  y=(l[1]+l[3])/2 - vp.y;
  if((x>0 && y > 0)|| (x<0 && y<0))
  {
      if(theta[i] >=15 && theta[i]<=75)
      {
        
       lines1.push_back(l);
      }
  }
  else if((x>0 && y < 0) || (x<0 && y>0))
  {
      if(theta[i] >=105 && theta[i]<=165)
      {
         lines1.push_back(l);

      }
  }
}
return lines1;
}
Point3f computevp(vector<Vec4i> lines1,Mat I)
{
int row=I.rows;
int col=I.cols;
//int *acc;
//acc = new int[row*col]();
int acc[row*col];
for(int itr=0;itr<row*col;itr++)
acc[itr]=0;
int Grid_No = 5;
int grid_x = floor(row/Grid_No);
int grid_y = floor(col/Grid_No);
  int cnt=lines1.size();
  for(int i=0;i<cnt;i++)
{
  Vec4i l;
  l=lines1[i];
  //line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
 // std::ostringstream str;
//str << i;
 // putText(Img, str.str(), cvPoint(l[0],l[1]),FONT_HERSHEY_COMPLEX,0.5,cvScalar(0,255,0),1,CV_AA);
for(int k=i+1;k<cnt;k++)
{

Point2f pt = lineintersect(lines1[i],lines1[k]);
if(round(pt.x) > 0 && round(pt.x) <col-1 && round(pt.y)>0 && round(pt.y) <row-1 )
{
int x=round(pt.x);
int y=round(pt.y);
//circle(Img,Point(pt.x,pt.y),2,Scalar(255,0,0),-1);
acc[y*col+x]+=1;
}
}
}
int ind=-1;
int val=0;
int cnt1=0;
int ind1=-1;

int gridacc[Grid_No*Grid_No];
for(int itr=0;itr<Grid_No*Grid_No;itr++)
gridacc[itr]=0;
for(int m=0;m<=row-grid_x;m=m+grid_x)
{
for(int q=0;q<=col-grid_y;q=q+grid_y)
{
for(int l=0;l<grid_x;l++)
{
for(int n=0;n<grid_y;n++)
{
ind1=col*m+q+(l*col)+n;
//cout << ind1 << endl;
gridacc[cnt1]+=acc[ind1];
}
}
cnt1++;
}
}
//Draw grid

/*for(int q=0;q<cnt1;q++)
{
int x1,y1;
x1=grid_y*(q%Grid_No);
y1=grid_x*(q/Grid_No);
line( Img, Point(0, y1), Point(col, y1), Scalar(255,255,0), 1, CV_AA);
line( Img, Point(x1, 0), Point(x1, row), Scalar(255,255,0), 1, CV_AA);
}*/
double N_int=0;
for(int q=0;q<cnt1;q++)
{
N_int=N_int+gridacc[q];
//cout << gridacc[q] << endl;
if(gridacc[q]>val)
{
val=gridacc[q];
ind=q;
}
}
double conf = val/(N_int+5);
//cout << "Confidence " << confidence << endl;
double ybar_gr=grid_x*(ind/Grid_No);
double xbar_gr=grid_y*(ind%Grid_No);
int q=ind%Grid_No;
int m=ind/Grid_No;
double myx=0,myy=0;
for(int l=0;l<grid_x;l++)
{
for(int n=0;n<grid_y;n++)
{
ind1=col*m*grid_x+(q*grid_y)+(l*col)+n;
myx+=(ybar_gr+l)*acc[ind1];
myy+=(xbar_gr+n)*acc[ind1];
}
}
myx=myx/val;
myy=myy/val;
int x_bar=myy;
int y_bar=myx;
Point3f pt1;
pt1.x=x_bar;
pt1.y=y_bar;
pt1.z=conf;
if(x_bar <0 || y_bar <0)
{
pt1.x=-1;
pt1.y=-1;
}
return pt1;
}

Point3f vanishpoint_script2_func(Mat Img,int frame, double* image)
{

Mat I,I_edge;
cvtColor(Img,I,CV_BGR2GRAY);
int row=I.rows;
int col=I.cols;
vector<double> mytheta; 
int Grid_No = 5;
int grid_x = floor(row/Grid_No);
int grid_y = floor(col/Grid_No);
//blur( I, I_edge, Size(3,3) );
//Canny(I_edge, I_edge, 40, 120);
vector<Vec4i> lines;
vector<Vec4i> lines1;
 int cnt=0;
//HoughLinesP(I_edge, lines, 1, CV_PI/180, 20, 10, 10 );
 // convert image to single array to be used as input to lsd 
for (int i=0;i<col;i++){
    for(int j=0;j<row;j++){
        image[ i + j * col ] = I.at<uchar>(j,i);
    }
    }
double *out;
    int n;
    out = lsd_scale( &n, image, col, row,0.4);

  for( size_t i = 0; i < n; i++ )
  {
  	Point2d pt1 = Point(out[ 0 + i * 7 ],out[ 1 + i * 7 ]); //end point1 of line
    Point2d pt2 = Point(out[ 2 + i * 7 ],out[ 3 + i * 7 ]); //end point2 of line
Vec4i l;
if(pt1.y<pt2.y)
{
l[0]=pt1.x; 
l[1]=pt1.y;
l[2]=pt2.x;
l[3]=pt2.y;
}
else
{
l[0]=pt2.x; 
l[1]=pt2.y;
l[2]=pt1.x;
l[3]=pt1.y;
}
double theta=0;

float num,den;

num = l[3]-l[1];
den=l[2]-l[0];
//line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,0,0), 5, CV_AA);
    theta= (atan2(num,den)*180/CV_PI);
    int len=sqrt(num*num+den*den);
   // cout << "before " << theta << endl;
    if(((abs(theta) >= 15 && abs(theta) <= 75) || (abs(theta) >=105 && abs(theta)<=165)) && len>30 )
{
    //cout << "after " << theta << endl;
    lines1.push_back(l);
    mytheta.push_back(theta);
    //line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
cnt=cnt+1;
}
  }
for(int i=0;i<lines1.size();i++)
{
  Vec4i l=lines1[i];
  line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
}
Point3f pt1=computevp(lines1,I);
//cout << lines1.size() << endl;
//cout << "bef " << pt1 << endl;
circle(Img,Point(pt1.x,pt1.y),7,Scalar(0,0,255),-1);
lines=linesupport(pt1,lines1,mytheta,Img);
for(int i=0;i<lines.size();i++)
{
  Vec4i l=lines[i];
  line( Img, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,0,0), 1, CV_AA);
}
pt1=computevp(lines,I);
//cout << lines.size() << endl;
//cout << "after " << pt1 << endl;
I.release();
Img.release();
I_edge.release();

return pt1;
}

