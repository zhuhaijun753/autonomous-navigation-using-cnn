
/* VIMOSTAR */
/****************************************************************************
 **
 ** File: <Kalman.h>
 **                                                                         **
 ** Copyright © 2014, Uurmi Systems                                         **
 ** All rights reserved.                                                    **
 ** http://www.uurmi.com                                                    **
 **                                                                         **
 ** All information contained herein is property of Uurmi Systems           **
 ** unless otherwise explicitly mentioned.                                  **
 **                                                                         **
 ** The intellectual and technical concepts in this file are proprietary    **
 ** to Uurmi Systems and may be covered by granted or in process national   **
 ** and international patents and are protect by trade secrets and          **
 ** copyright law.                                                          **
 **                                                                         **
 ** Redistribution and use in source and binary forms of the content in     ** 
 ** this file, with or without modification are not permitted unless        ** 
 ** permission is explicitly granted by Uurmi Systems.                      **
 **                                                                         **
 ****************************************************************************/
/* Header function for Kalman function */

#pragma once
#include "opencv2/opencv.hpp"
#include <opencv/cv.h>
using namespace cv;
using namespace std;
class TKalmanFilter
{
public:
	KalmanFilter* kalman;
	double deltatime; 
	Point2f LastResult;
	TKalmanFilter(Point2f p,float dt);
	~TKalmanFilter();
	Point2f GetPrediction(Mat H,int flag);
	Point2f Update(Point2f p, bool DataCorrect);
};

