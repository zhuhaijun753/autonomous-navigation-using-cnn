#include<iostream>
#include "classifierann.hpp"
using namespace std;
using namespace cv;

Point2f classifierann(Mat Img1)
{
    int descsize = 0;
    int nblocks = 4;
    int n_scale = 4;
    int orientations_per_scale[50] = { 8, 8, 8, 8 };
    

    Mat temp;
    ofstream myfile;
    myfile.open("mytest.txt");
    //color_image_t *im = load_ppm(path.c_str());

    Mat Img2=Mat::zeros(Img1.size(),CV_32FC1);
    for (int i = 0; i < Img1.rows; i++) {
        for (int j = 0; j < Img1.cols; j++) {
            Vec3b rgb = Img1.at<Vec3b>(i, j);
            float b = rgb[0];
            float g = rgb[1];
            float r = rgb[2];
            Img2.at<float>(i,j)=float((b+g+r)/3);
            //   cout << Im->data[l] << endl;

        }
    }
    Size size(256,256);
    Mat Img=Mat::zeros(256,256,CV_32FC1);
    resize(Img2,Img,size,CV_INTER_LINEAR);
    //cout << Img << endl;
    //color_image_t *Im = color_image_new(Img.cols, Img.rows);
    image_t *Im = image_new(Img.cols, Img.rows);
    // Image assertion
    assert(Im);
    int l = 0;
    for (int i = 0; i < Img.rows; i++) {
        for (int j = 0; j < Img.cols; j++) {
            Im->data[l]=Img.at<float>(i,j);
            //   cout << Im->data[l] << endl;
            l++;
        }
    }
    // End image assertion

    float *desc = bw_gist_scaletab(Im, nblocks, n_scale,
                                   orientations_per_scale);

    for (int i = 0; i < n_scale; i++)
        descsize += nblocks * nblocks * orientations_per_scale[i];

    descsize *= 1; /* color */

    int arr[4]={0};
    arr[0]=1;
    myfile << 1 << " " << 512 << " " << 4 << endl;
    for (int i = 0; i < descsize; i++) {
        //myfile << " " << i + 1;
        myfile << desc[i] << " ";
    }
    myfile << endl;
    for(int i=0;i<4;i++)
        myfile << arr[i] << " ";
    myfile << endl;
    myfile.close();
    struct fann *ann = fann_create_from_file("/home/kbipin/catkin_ws/src/ardrone_perception/data/train1.net");
    struct fann_train_data *test_data;
    fann_type *calc_out;
    //fann_type *input;
    test_data = fann_read_train_from_file("mytest.txt");
    


    calc_out = fann_run(ann, test_data->input[0]);
    float max=-100.00;
    int ind=-1;
    for(int j=0;j<4;j++)
    {
        if(calc_out[j]>max)
        {
            ind = j;
            max=calc_out[j];
        }
    }
    Point2f pt;
    pt.x=ind;
    pt.y=max;

    return pt;
}
