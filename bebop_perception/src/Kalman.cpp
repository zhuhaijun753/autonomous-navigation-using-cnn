
/* VIMOSTAR */
/****************************************************************************
 **
 ** File: <Kalman.cpp>
 **                                                                         **
 ** Copyright © 2014, Uurmi Systems                                         **
 ** All rights reserved.                                                    **
 ** http://www.uurmi.com                                                    **
 **                                                                         **
 ** All information contained herein is property of Uurmi Systems           **
 ** unless otherwise explicitly mentioned.                                  **
 **                                                                         **
 ** The intellectual and technical concepts in this file are proprietary    **
 ** to Uurmi Systems and may be covered by granted or in process national   **
 ** and international patents and are protect by trade secrets and          **
 ** copyright law.                                                          **
 **                                                                         **
 ** Redistribution and use in source and binary forms of the content in     **
 ** this file, with or without modification are not permitted unless        **
 ** permission is explicitly granted by Uurmi Systems.                      **
 **                                                                         **
 ****************************************************************************/
/* This function uses opencv Kalman filter for prediction and updation of tracks */

#include "Kalman.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
using namespace cv;
using namespace std;
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
TKalmanFilter::TKalmanFilter(Point2f pt,float dt) //Initialize Parameters of Kalamn Filter
{

    deltatime = dt; //0.2

    kalman = new KalmanFilter( 4, 2, 0 );

    kalman->transitionMatrix = (Mat_<float>(4, 4) << 1,0, 1, 0,   0,1,0,1,  0,0,1,0,  0,0,0,1);

    // init...
    LastResult = pt;
    kalman->statePre.at<float>(0) = pt.x; // x
    kalman->statePre.at<float>(1) = pt.y; // y

    kalman->statePre.at<float>(2) = 0;
    kalman->statePre.at<float>(3) = 0;

    kalman->statePost.at<float>(0)=pt.x;
    kalman->statePost.at<float>(1)=pt.y;

    setIdentity(kalman->measurementMatrix);
    kalman->processNoiseCov=(Mat_<float>(4, 4) <<
                             pow(deltatime, 4.0)/4.0	,0						,pow(deltatime,3.0)/2.0		,0,
                             0						,pow(deltatime,4.0)/4.0	,0							,pow(deltatime,3.0)/2.0,
                             pow(deltatime,3.0)/2.0	,0						,pow(deltatime,2.0)			,0,
                             0						,pow(deltatime,3.0)/2.0	,0							,pow(deltatime,2.0));

    setIdentity(kalman->measurementNoiseCov, Scalar::all(0.1));
    setIdentity(kalman->errorCovPost, Scalar::all(.1));

}
//---------------------------------------------------------------------------
TKalmanFilter::~TKalmanFilter() // Delete Kalman Filter
{
    delete kalman;
}

//---------------------------------------------------------------------------
Point2f TKalmanFilter::GetPrediction(Mat H,int flag) // predict the next state 
{       
    if(flag==1)
    {
        Mat prediction = kalman->predict();
        LastResult=Point2f(prediction.at<float>(0),prediction.at<float>(1));
    }
    else if(fabs(H.at<double>(0, 0)) > 2 || fabs(H.at<double>(0, 0)) < 0.6 || fabs(H.at<double>(1, 1)) > 2 || fabs(H.at<double>(1, 1)) < 0.6 || fabs(H.at<double>(2, 2)) > 2 || fabs(H.at<double>(2, 2)) < 0.6)
    {
    }
    else if(countNonZero(H) >= 1)
    {
        double x = LastResult.x;
        double y = LastResult.y;
        double h[9];
        h[0]=H.at<double>(0,0);
        h[1]=H.at<double>(0,1);
        h[2]=H.at<double>(0,2);
        h[3]=H.at<double>(1,0);
        h[4]=H.at<double>(1,1);
        h[5]=H.at<double>(1,2);
        h[6]=H.at<double>(2,0);
        h[7]=H.at<double>(2,1);
        h[8]=H.at<double>(2,2);
        double Z = 1./(h[6]*x + h[7]*y + h[8]);
        LastResult.x = (int)((h[0]*x + h[1]*y + h[2])*Z);
        LastResult.y = (int)((h[3]*x + h[4]*y + h[5])*Z);
    }
    return LastResult;
}
//---------------------------------------------------------------------------
Point2f TKalmanFilter::Update(Point2f p, bool DataCorrect) // Update the state of filter
{
    Mat measurement(2,1,CV_32FC1);
    if(!DataCorrect) // If no detections, use predictions
    {
        measurement.at<float>(0) = LastResult.x;
        measurement.at<float>(1) = LastResult.y;
    }
    else
    {
        measurement.at<float>(0) = p.x;
        measurement.at<float>(1) = p.y;
    }
    Mat estimated = kalman->correct(measurement); // Correct the state
    LastResult.x=estimated.at<float>(0);
    LastResult.y=estimated.at<float>(1);
    return LastResult;
}
//---------------------------------------------------------------------------
