#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

import math
import sys
import os
import time

import numpy as np
import theano
import theano.tensor as T

import lasagne
import cPickle

#f = file('/home/kbipin/catkin_bebop/src/cnn_planner/model/CNN_Model_NEW_VINDHYA.save', 'rb')
#f = file('/home/kbipin/catkin_bebop/src/cnn_planner/model/CNN_Model_NEW_NBH_4_OPTIONS_Ver2.save', 'rb')
f = file('/home/kbipin/catkin_bebop/src/cnn_planner/model/CNN_Model_NEW_NBH_4_OPTIONS.save', 'rb')
network = cPickle.load(f)
f.close()

# Prepare Theano variables for inputs and targets
input_var = T.tensor4('inputs')
target_var = T.ivector('targets')

# Create a loss expression for validation/testing. The crucial difference
# here is that we do a deterministic forward pass through the network,
# disabling dropout layers.
test_prediction = lasagne.layers.get_output(network,inputs=input_var, deterministic=True)
test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
test_loss = test_loss.mean()
# As a bonus, also create an expression for the classification accuracy:
test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                      dtype=theano.config.floatX)

# Compile a second function computing the validation loss and accuracy:
val_fn = theano.function([input_var, target_var],[test_prediction, test_loss, test_acc])
# After training, we compute and print the test error:
test_err = 0
test_acc = 0
test_batches = 1
count = 0


class image_converter:
  def __init__(self):
    self.image_pub = rospy.Publisher("/flightPlanning/cmd", String, queue_size=1)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/depth_estimation/image", Image, self.callback, queue_size = 1)


  def callback(self,data):
    global count
    start_time = time.time()
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)
    
    cv_image = cv2.cvtColor( cv_image, cv2.COLOR_RGB2GRAY )
    cv_image = np.asarray(cv_image, dtype='float64') / 19.
    
    inputs = cv_image.reshape(-1, 1, 364, 636)
    targets = [0]

    predict, err, acc = val_fn(inputs, targets)
    
    rate = rospy.Rate(50)

    p0 = np.argmax(predict)
    #print(predict)
    if predict.all() < 0.85:
	command_str = "Hover"
        rospy.loginfo(command_str)
        self.image_pub.publish(command_str)
        rate.sleep()
    elif p0 == 0 or count > 0:
        command_str = "GoStraight"
        rospy.loginfo(command_str)
        self.image_pub.publish(command_str)
        rate.sleep()
    elif p0 == 1 and count == 0:
        command_str = "Explore"
        rospy.loginfo(command_str)
        self.image_pub.publish(command_str)
        rate.sleep()
    elif p0 == 2 and count == 0:
        command_str = "MoveLeft"
        rospy.loginfo(command_str)
        self.image_pub.publish(command_str)
        rate.sleep()
    elif p0 == 3 and count == 0:
        command_str = "MoveRight"
        rospy.loginfo(command_str)
        self.image_pub.publish(command_str)
        rate.sleep()
    else:
        command_str = "Hover"
        rospy.loginfo(command_str)
        self.image_pub.publish(command_str)
        rate.sleep()
    #print(start_time-time.time())
def main(args):  

  rospy.init_node('image_converter', anonymous=True)
  ic = image_converter()

  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
